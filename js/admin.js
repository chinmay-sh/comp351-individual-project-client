const addOptions = (e) => {
    let container = document.getElementById(`optionsContainer-add`);

    let optionNum = container.childNodes.length - 2;

    console.log(optionNum)
    if (optionNum > 4) {
        let x = document.getElementById(`optionButtons-add`);
        x.innerHTML = `
        <div class="alert alert-warning alert-dismissible" role="alert">
            Max <strong>4</strong> options allowed!
        </div>
        `;
    } else {
        let newDiv = document.createElement("DIV");
        newDiv.setAttribute("class", "form-group row");
        newDiv.innerHTML = `
        <div class="input-group mb-3">
            <input required="required" type="text" name="option${optionNum}text-add"
                placeholder="Option ${optionNum}" class="form-control" id="option${optionNum}text-add">
            <span class="input-group-text" id="basic-addon${optionNum}-add">Correct: &nbsp;
                <input class="form-check-input" type="radio" value="${optionNum}"
                    id="option${optionNum}radio-add" name="question-radio-add"
                    required="required"></span>
        </div>
        `
        container.appendChild(newDiv);
    }

}


function resetForms() {
    document.getElementById('addform').reset();
    document.getElementById('editform').reset();
}

const editFormLoader = (qid, qbody, corOpt, optionList) => {

    document.getElementById('questionArea-edit').innerHTML = qbody;

    let radios = document.getElementsByName('question-radio-edit');
    radios[corOpt].checked = true;

    for (let i = 1; i <= optionList.length; i++) {
        document.getElementById(`option${i}text-edit`).value = optionList[i - 1].optvalue;
    }


    document.getElementById('editformSubmit').addEventListener("click", () => { editQuestion(qid) });
}

const editQuestion = (qid) => {
    let quesBody = document.getElementById('questionArea-edit').value;

    let no_of_options = document.getElementById(`optionsContainer-edit`).childNodes.length - 5;

    console.log(no_of_options);

    let answerArr = [];

    for (let i = 1; i <= no_of_options; i++) {
        let val = document.getElementById(`option${i}text-edit`).value;
        if (val != null && val != "") {
            answerArr.push(val)
        }
    }

    let radios = document.getElementsByName('question-radio-edit');
    let radioVal = 0;

    for (let i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            radioVal = radios[i].value;
            console.log("Radio Value: " + radioVal)
            break;
        }
    }

    if (quesBody == "" || answerArr.length <= 1 || radioVal == 0 || radioVal > answerArr.length) {
        alert("Enter valid values for all fields.")
        return;
    }

    let formData = {
        "quesBody": quesBody,
        "correctOption": radioVal - 1,
        "optionList": answerArr
    }

    fetch('https://individual-project-server-wkh4p.ondigitalocean.app/questions/' + qid, {
        method: 'PUT',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => response).then(data => {
        console.log(data);
        location.reload();
    });

    resetForms();

}


const addQuestion = () => {

    let quesBody = document.getElementById('questionArea-add').value;


    let no_of_options = document.getElementById(`optionsContainer-add`).childNodes.length - 2;

    let answerArr = [];

    for (let i = 1; i < no_of_options; i++) {
        let val = document.getElementById(`option${i}text-add`).value;
        if (val != null && val != "") {
            answerArr.push(val)
        }
    }


    let radios = document.getElementsByName('question-radio-add');
    let radioVal = 0;

    for (let i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            radioVal = radios[i].value;
            console.log("Radio Value: " + radioVal)
            break;
        }
    }

    if (quesBody == "" || answerArr.length <= 1 || radioVal == 0 || radioVal > answerArr.length) {
        alert("Enter valid values for all fields.")
        return;
    }

    let formData = {
        "quesBody": quesBody,
        "correctOption": radioVal - 1,
        "optionList": answerArr
    }

    fetch('https://individual-project-server-wkh4p.ondigitalocean.app/questions', {
        method: 'post',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => response).then(data => {
        console.log(data);
        location.reload();
    });

    resetForms();

}


function getOptions(optionsList, qNum, correctANS) {
    let stringOut = ""
    for (let i = 0; i < optionsList.length; i++) {
        if (optionsList[i].optionNum == correctANS) {
            stringOut += `<input type="radio" name="ques${qNum}" value="1" disabled checked /> ${i + 1}) <span style="color:green">${optionsList[i].optvalue} -->> CORRECT</span> <br />`
        } else {
            stringOut += `<input type="radio" name="ques${qNum}" value="1" disabled /> ${i + 1}) ${optionsList[i].optvalue} <br />`
        }
    }
    return stringOut;
}

function deleteQuestion(qid) {
    if (confirm('Are you sure you want to delete the question?')) {
        // Delete!
        console.log('Question deleted with ID: ' + qid);
        fetch('https://individual-project-server-wkh4p.ondigitalocean.app/questions/' + qid, {
            method: 'delete',
            mode: 'cors',
            cache: 'no-cache'
        }).then(response => response).then(data => {
            console.log(data);
            location.reload();
        });
    } else {
        // skip
        console.log('delete cancelled');
    }
}

function textAreaAdjust() {
    let x = document.getElementsByClassName("loadedQuesTA");
    let i;
    for (i = 0; i < x.length; i++) {
        x[i].style.height = "1px";
        x[i].style.height = (1 + x[i].scrollHeight) + "px";
    }
}

function dispData() {
    fetch('https://individual-project-server-wkh4p.ondigitalocean.app/questions')
        .then(response => response.json())
        .then(data => {

            let output = document.getElementById('output');

            for (let i = 0; i < data.length; i++) {
                console.log(data[i]);
                output.insertAdjacentHTML('beforeend', `
            <div class="question">
                <h4 class="number">
                    Question ${i + 1}
                    <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#editQuestionModal" id="editButton-${i}">
                    Edit
                    </button>
                    <button type="button" class="btn btn-danger" id="deleteButton-${i}">
                    Delete
                    </button>
                </h4>

                <div>
                    <textarea id="textarea${i}"  readonly class="form-control loadedQuesTA" placeholder="No question" name="Question-textarea-${i}">${data[i].questionBody}</textarea>
                </div>
                <div>
                    
                    <form name="ques${i}form" id="q${i}radio">
                        ${getOptions(data[i].options, i, data[i].correctOpt)}
                    </form>
                </div>
                <div id="alertHolder-${i}">

                </div>
            </div>
            `);
                document.getElementById(`editButton-${i}`).addEventListener("click", () => {
                    editFormLoader(data[i].id, data[i].questionBody, data[i].correctOpt, data[i].options);
                });

                document.getElementById(`deleteButton-${i}`).addEventListener("click", () => {
                    deleteQuestion(data[i].id);
                });
            }
            textAreaAdjust();
        });

}


dispData();