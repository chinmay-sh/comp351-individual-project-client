let correct = 0;
let totalScore = 0;


function textAreaAdjust() {
    let x = document.getElementsByTagName("TEXTAREA");
    let i;
    for (i = 0; i < x.length; i++) {
        x[i].style.height = "1px";
        x[i].style.height = (1 + x[i].scrollHeight) + "px";
    }
}

function radioDisable(radioNum) {
    let rad = document.getElementById(`q${radioNum}radio`);
    for (let i = 0; i < rad.length; i++) {
        rad[i].disabled = true;
    }
}

function radioEvent(quesNum, correctAns) {
    let rad = document.getElementById(`q${quesNum}radio`);
    let prev = null;

    for (let i = 0; i < rad.length; i++) {
        rad[i].addEventListener('change', function () {
            (prev) ? console.log(`Previous Val: ${prev.value}`) : null;
            if (this !== prev) {
                prev = this;
            }
            console.log(this.value);
            console.log('Correct Option:' + correctAns)

            radioDisable(quesNum);

            let myAlert = document.getElementById('alertHolder-' + quesNum)
            if (this.value == correctAns) {
                myAlert.innerHTML = `<div class="alert alert-success alert-dismissible" role="alert">
                    <strong>Correct Answer!</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`;
                correct++;
                scoreCard();
            } else {
                myAlert.innerHTML = `<div class="alert alert-danger alert-dismissible" role="alert">
                    <strong>Wrong Answer!</strong> The correct answer is ${correctAns + 1}.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`;
            }
        });
    }
}

function getOptions(optionsList, qNum) {
    let stringOut = ""
    for (let i = 0; i < optionsList.length; i++) {
        stringOut += `<input type="radio" name="ques${qNum}" value="${i}" /> ${i + 1}) ${optionsList[i].optvalue} <br />`
    }
    return stringOut;
}

document.addEventListener('DOMContentLoaded', function (event) {
    let holder = document.getElementById("quizHolder_student");
    fetch('https://individual-project-server-wkh4p.ondigitalocean.app/questions')
        .then(response => response.json())
        .then(data => {
            console.log(data)
            let quesData_parsed = null;
            if (data == null || data.length == 0) {
                holder.innerHTML = `<div class="col-12 alert alert-danger" role="alert">
                                        <strong>Please Add Some questions!</strong>
                                    </div>`;
                return;
            } else {
                for (let i = 0; i < data.length; i++) {
                    quesData_parsed = data;
                }
            }
            console.log((quesData_parsed));
            for (let i = 0; i < quesData_parsed.length; i++) {
                totalScore = quesData_parsed.length;
                holder.insertAdjacentHTML('beforeend', `
                <div class="question">
                    <h4 class="number">
                        Question ${i + 1}                    
                    </h4>
    
                    <div>
                        <textarea id="textarea${i}"  readonly class="form-control" placeholder="No question" name="Question-textarea-${i}">${data[i].questionBody}</textarea>
                    </div>
                    <div>
                        
                        <form name="ques${i}form" id="q${i}radio">
                            ${getOptions(data[i].options, i)}
                        </form>
                    </div>
                    <div id="alertHolder-${i}">
    
                    </div>
                </div>
                `);
                radioEvent(i, quesData_parsed[i].correctOpt);
            }
            textAreaAdjust();


            scoreCard();

        })
});

function scoreCard() {
    let resultholder = document.getElementById("result_holder");
    resultholder.innerHTML = `<strong>SCORE: ${correct}/${totalScore}</strong>`
}